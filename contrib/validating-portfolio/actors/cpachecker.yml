# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

imports:
  - !include resource_limits.yml
actor_name: cpachecker
toolinfo_module: "https://gitlab.com/sosy-lab/software/benchexec/-/raw/main/benchexec/tools/cpachecker.py"
archives:
  - version: default
    options: [ '-svcomp22', '-heap', '10000M', '-benchmark', '-timelimit', '900 s' ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    spdx_license_identifier: "Apache-2.0"
  - version: validator
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
  - version: validator-2-units
    spdx_license_identifier: "Apache-2.0"
    options: ['-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}"]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-7500MB.yml
  - version: validator-4-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-3750MB.yml
  - version: validator-6-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-2500MB.yml
  - version: validator-8-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-1875MB.yml
  - version: validator-12-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-1250MB.yml
  - version: validator-16-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-938MB.yml
  - version: validator-32-units
    spdx_license_identifier: "Apache-2.0"
    options: [ '-witnessValidation', '-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'cpa.predicate.memoryAllocationsAlwaysSucceed=true', '-setprop', 'cpa.smg.memoryAllocationFunctions=malloc,__kmalloc,kmalloc,kzalloc,kzalloc_node,ldv_zalloc,ldv_malloc', '-setprop', 'cpa.smg.arrayAllocationFunctions=calloc,kmalloc_array,kcalloc', '-setprop', 'cpa.smg.zeroingMemoryAllocation=calloc,kzalloc,kcalloc,kzalloc_node,ldv_zalloc', '-setprop', 'cpa.smg.deallocationFunctions=free,kfree,kfree_const', "-witness", "${witness}" ]
    location: "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip"
    resourcelimits: !include resource-limitations-900sec-469MB.yml
format_version: '1.2'
