// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Portfolio

// Create verifier from external-actor definition file
cpachecker = ActorFactory.create(ProgramVerifier, "../actors/cpa-seq.yml", "default");
symbiotic = ActorFactory.create(ProgramVerifier, "../actors/symbiotic.yml", "default");
uautomizer = ActorFactory.create(ProgramVerifier, "../actors/uautomizer.yml", "default");
esbmc_kind = ActorFactory.create(ProgramVerifier, "../actors/esbmc-kind.yml", "default");

// Prepare example inputs
prog = ArtifactFactory.create(Program, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':prog, 'spec':spec};

success_condition = ELEMENTOF(verdict, {TRUE,FALSE});

portfolio = PARALLEL_PORTFOLIO(cpachecker,symbiotic,uautomizer,esbmc_kind,success_condition);

print(portfolio);

result = execute(portfolio, inputs);
print(result);