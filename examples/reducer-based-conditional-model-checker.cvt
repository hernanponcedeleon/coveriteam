// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Reducer-Based Construction of a Conditional Model Checker

verifier = ActorFactory.create(ProgramVerifier, "../actors/uautomizer.yml", "default");
reducer = ActorFactory.create(CMCReducer, "../actors/cmc-reducer.yml", "default");
cmc = SEQUENCE(reducer, verifier);

// Print type information about the composition (for illustration)
print("\nFollowing is the type of the actor cmc");
print(cmc);

// Prepare test inputs.
cond = ArtifactFactory.create(Condition, cond_path);
prog = ArtifactFactory.create(CProgram, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
ip = {'program':prog, 'spec':spec,'condition':cond};

// Execute the component on the inputs.
res = execute(cmc, ip);
print("The following artifacts were produced by the execution:");
print(res);
