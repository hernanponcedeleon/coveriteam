// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Execution-Based Validation

w2test = ActorFactory.create(WitnessToTest, "../actors/cpachecker-witness-to-test.yml", "default");
testval = ActorFactory.create(TestValidator, "../actors/test-val.yml", "default");
spec_to_testspec = SpecToTestSpec();
execution_based_validator = SEQUENCE(PARALLEL(w2test, spec_to_testspec), testval);

// Prepare test inputs.
witness = ArtifactFactory.create(ReachabilityWitness, witness_path);
prog = ArtifactFactory.create(CProgram, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
ip = {'program':prog, 'spec':spec, 'witness':witness};

// Print type information about the composition (for illustration)
print("\nFollowing is the type of the actor execution_based_validator:");
print(execution_based_validator);

// Execute the actor on the inputs.
res = execute(execution_based_validator, ip);
print("The following artifacts were produced by the execution:");
print(res);

