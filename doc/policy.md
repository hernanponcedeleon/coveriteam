<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Policies in CoVeriTeam

CoVeriTeam supports use of a policy to restrict the urls from where actors and tool info modules could be downloaded.

This YAML file describes the syntax of a YAML file describing such a policy.

`allowed_locations`: a list of locations from which the downloads are allowed. 
A tool archive or a tool info module can only be downloaded if it matches at least one of the urls in this list.
These urls are allowed prefixes of the actual allowed url.

No restrictions are applied in case no policy file is defined.
